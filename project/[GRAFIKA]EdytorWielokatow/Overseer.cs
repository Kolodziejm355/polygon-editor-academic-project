﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Dynamic;
using System.Runtime.Remoting.Messaging;
using System.Windows.Forms;

namespace _GRAFIKA_EdytorWielokatow
{
    public enum WorkMode
    {
        Create,
        Selection,
        Move,
        Clip,
        NoMode
    }

    public enum Direction
    {
        Up,
        Right,
        Down,
        Left,
        Above,
        Below
    }

    //a class which purpose is to own polygons and provide their API
    public static class Overseer
    {
        static readonly List<Polygon> Polygons = new List<Polygon>();
        static UnfinishedPolygon _unfinishedOne;

        public static WorkMode SelectedWorkMode { get; set; } = WorkMode.NoMode;

        static void AddPolygon(Polygon p)
        {
            Polygons.Add(p);
        }
        static void RemovePolygon(Polygon p)
        {
            Polygons.Remove(p);
            _selected = null;
        }


        //API for drawing

        public static Bitmap DrawPolygons(Bitmap bitmap, Image image, Color[] colors, Color[][] texture, Color lightColor)
        {
            foreach (var p in Polygons)
            {
                p.ScanLineAlgorithm(bitmap, lightColor);
                p.Draw(bitmap, colors[0], colors[0]); //points' color must be black for boundary fill to work
            }
            _unfinishedOne?.Draw(bitmap, colors[0], colors[1]);
            DrawSelected(bitmap, colors[2], colors[3]);
            return bitmap;
        }

        public static void DrawSelected(Bitmap bitmap, Color segmentColor, Color pointColor)
        {
            if (_selected == null) return;
            (_selected as Point)?.Draw(bitmap, pointColor);
            (_selected as LinkedListNode<Segment>)?.Value.Draw(bitmap, segmentColor, pointColor);
            (_selected as Polygon)?.Draw(bitmap, segmentColor, pointColor);
        }

        //API for textures
        public static void ChooseTexture(Color[][] texture)
        {
            if (_selected == null)
                return;

            var p = _selected as Polygon;
            if (p != null)
                p.Texture = texture;
            else
            {
                var s = _selected as LinkedListNode<Segment>;
                if (s != null)
                    s.Value.Start.Owner.Texture = texture;
                else
                {
                    var point = _selected as Point;
                    if (point != null)
                        point.Owner.Texture = texture;
                }
            }
        }

        public static void ChooseNormalMap(Vector3[][] normals)
        {
            if (_selected == null)
                return;

            var p = _selected as Polygon;
            if (p != null)
                p.NormalVectors = normals;
            else
            {
                var s = _selected as LinkedListNode<Segment>;
                if (s != null)
                    s.Value.Start.Owner.NormalVectors = normals;
                else
                {
                    var point = _selected as Point;
                    if (point != null)
                        point.Owner.NormalVectors = normals;
                }
            }
        }

        public static void ChooseHeightMap(Vector3[][] bumps)
        {
            if (_selected == null)
                return;

            var p = _selected as Polygon;
            if (p != null)
                p.BumpMap = bumps;
            else
            {
                var s = _selected as LinkedListNode<Segment>;
                if (s != null)
                    s.Value.Start.Owner.BumpMap = bumps;
                else
                {
                    var point = _selected as Point;
                    if (point != null)
                        point.Owner.BumpMap = bumps;
                }
            }
        }

        public static double Offset = 50;
        public static void MoveLight(Direction d)
        {
            switch (d)
            {
                case Direction.Left:
                    Geometry.LightPos.x -= Offset;
                    break;
                case Direction.Right:
                    Geometry.LightPos.x += Offset;
                    break;
                case Direction.Up:
                    Geometry.LightPos.y -= Offset;
                    break;
                case Direction.Down:
                    Geometry.LightPos.y += Offset;
                    break;
                case Direction.Above:
                    Geometry.LightPos.z += Offset;
                    break;
                case Direction.Below:
                    Geometry.LightPos.z = Geometry.LightPos.z - Offset < 50 ? 50 : Geometry.LightPos.z - Offset;
                    break;
            }
        }

        //API for moving point, selecting segment

        //for selected point/segment/polygon
        private static object _selected;
        //do we select point/segment or whole polygon
        public static bool SelectWhole = false;


        //API for selecting

        public static void ClearSelection()
        {
            _selected = null;
        }

        //if selectWhole, then select polygon
        public static void SelectPoint(int x, int y)
        {
            Point from = new Point(x, y);
            const int validPointDistance = 8;
            foreach (var p in Polygons)
            {
                object selectedTmp = p.GetValidPoint(from, validPointDistance);
                if (selectedTmp == null) continue;

                _selected = selectedTmp;
                if (SelectWhole)
                    _selected = p;
                return;
            }
        }

        //if selectWhole, then select polygon
        public static void SelectSegment(int x, int y)
        {
            Point from = new Point(x, y);
            int validSegmentDistance = 6;

            foreach (var p in Polygons)
            {
                _selected = p.GetValidSegment(from, validSegmentDistance);
                if (_selected != null)
                {
                    if (SelectWhole)
                        _selected = p;
                    return;
                }
            }
        }

        public static bool IsSegmentSelected()
        {
            return (_selected is LinkedListNode<Segment>);
        }


        //API for moving selected

        // if selectWhole, then it drags whole polygon
        public static void DragPoint(Point dest)
        {
            if (_selected != null && SelectedWorkMode == WorkMode.Move)
            {
                Point selected = _selected as Point;
                if (selected != null)
                {
                    selected.X = dest.X;
                    selected.Y = dest.Y;
                }
                else
                    (_selected as Polygon)?.MovePolygon(dest);
            }
        }

        public static void BeginMove()
        {
            if (_selected != null && (_selected is Point || _selected is Polygon))
                SelectedWorkMode = WorkMode.Move;
        }

        public static void StopMove()
        {
            if (SelectedWorkMode == WorkMode.Move)
                SelectedWorkMode = WorkMode.Selection;
        }

        //API for clipping
        public static void Clip(int x, int y)
        {
            Polygon selectedPolygon;
            Polygon selected = _selected as Polygon;
            if (selected != null)
                selectedPolygon = selected;
            else
            {
                Point selPoint = _selected as Point;
                if (selPoint != null)
                    selectedPolygon = selPoint.Owner;
                else
                    return;
            }

            Point from = new Point(x, y);
            const int validPointDistance = 8;
            foreach (var p in Polygons)
            {
                if (p == selectedPolygon)
                    continue;
                object selectedTmp = p.GetValidPoint(from, validPointDistance);
                if (selectedTmp == null) continue;

                List<Polygon> newOnes = selectedPolygon.ClipPolygon(p);
                _selected = null;
                Polygons.Remove(selectedPolygon);
                Polygons.Remove(p);
                foreach (var n in newOnes)
                    Polygons.Add(n);
                SelectedWorkMode = WorkMode.Selection;
                return;
            }
        }
        public static void BeginClip()
        {
            if (_selected != null && (_selected is Point || _selected is Polygon))
                SelectedWorkMode = WorkMode.Clip;
        }
        public static void StopClip()
        {
            if (SelectedWorkMode == WorkMode.Clip)
                SelectedWorkMode = WorkMode.Selection;
        }

        //API for removing selected

        public static void RemovePoint()
        {
            if (!(_selected is Point))
                return;
            Polygon p = ((Point)_selected).Owner;
            p.RemovePoint((Point)_selected);
            _selected = null;
        }

        public static void RemoveSelectedPolygon()
        {
            Polygon selected = _selected as Polygon;
            if (selected != null)
                RemovePolygon(selected);
        }
       

        //API for creating

        public static void CreateNewPoint(int fromX, int fromY)
        {
            if(!(_selected is LinkedListNode<Segment>))
                return;
            Point from = new Point(fromX, fromY);
            LinkedListNode<Segment> s = (LinkedListNode<Segment>) _selected;
            _selected = null;
            Point p = new Point(from.X, from.Y) {Owner = s.Value.Start.Owner};
            Segment s1 = new Segment(s.Value.Start, p);
            Segment s2 = new Segment(p,s.Value.End);
            if (s.Previous == null)
            {
                s.List.AddFirst(s2);
                s.List.AddFirst(s1);
                s.List.Remove(s);
                return;
            }
            s = s.Previous;
            if(s.Next == null)
                throw new Exception("Overseer.CreateNewPoint exception");
            s.List.Remove(s.Next);
            s.List.AddAfter(s, s2);
            s.List.AddAfter(s, s1);
        }
        

        //API for unfinished polygon

        public static void MoveUnfinishedEnd(int x, int y)
        {
            _unfinishedOne?.UpdateEnd(x,y);
        }

        public static void ResetUnfinishedOne()
        {
            _unfinishedOne = null;
        }

        public static void ConfirmPointUnfinished(int x, int y)
        {
            if(_unfinishedOne==null)
                _unfinishedOne = new UnfinishedPolygon(new Point(x,y));
            _unfinishedOne?.UpdateEnd(x, y);
            Polygon p = _unfinishedOne?.PointConfirmation();
            if (p != null)
            {
                AddPolygon(p);
                _unfinishedOne = null;
            }
        }
    }
}