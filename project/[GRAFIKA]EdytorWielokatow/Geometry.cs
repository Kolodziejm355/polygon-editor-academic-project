﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Windows.Forms.VisualStyles;

namespace _GRAFIKA_EdytorWielokatow
{
    public static class Geometry
    {
        public static int Epsilon = 2;
        public static double LambHelp = (double) 1/255;
        public static Vector3 LightPos = new Vector3(500, 500, 100);

        public static int Dist(Point p1, Point p2) // point 2 point dist (square) 
        {
            int d1 = p1.X - p2.X;
            int d2 = p1.Y - p2.Y;

            return d1 * d1 + d2 * d2;
        }

        public static int Dist(Point p, Segment s) // point 2 segment dist (not square)
        {
            int dot1 = DotProduct(s.Start, s.End, p);
            if (dot1 > 0)
                return Dist(s.End, p);

            int dot2 = DotProduct(s.End, s.Start, p);
            if (dot2 > 0)
                return Dist(s.Start, p);

            int cross = CrossProduct(s.Start, s.End, p);
            int distp2p = Dist(s.Start, s.End);
            if (distp2p <= 0)
                distp2p = 1;
            int dist =  cross * cross / distp2p;
            
            return Math.Abs(dist);
        }

        //dot product p12 . p13
        private static int DotProduct(Point p1, Point p2, Point p3)
        {
            Point p12 = new Point(p2.X - p1.X,p2.Y - p1.Y);
            Point p23 = new Point(p3.X - p2.X, p3.Y - p2.Y);
            int dot = p12.X * p23.X + p12.Y * p23.Y;

            return dot;
        }

        //cross product p12 x p13
        private static int CrossProduct(Point p1, Point p2, Point p3)
        {
            Point p12 = new Point(p2.X - p1.X, p2.Y - p1.Y);
            Point p13 = new Point(p3.X - p1.X, p3.Y - p1.Y);

            int cross = p12.X * p13.Y - p12.Y * p13.X;

            return cross;
        }
        
        public static double Cos(Vector3 a, Vector3 light)
        { //light is already normalized
            a.Normalize();

            double dotProduct = a.x* light.x + a.y* light.y + a.z* light.z;
            double lengthSum = 2;
            double cos = dotProduct / lengthSum;
            if (cos < 0)
                cos = 0;
            return cos;
        }

        public static void Bresenham(Point p1, Point p2, Color color, Bitmap bitmap)
        {
            int dx = p2.X - p1.X;
            int dy = p2.Y - p1.Y;
            int swaps = 0;

            if (Math.Abs(dy) > Math.Abs(dx))
            {
                Swap(ref dx, ref dy);
                swaps = 1;
            }

            int a = Math.Abs(dy);
            int b = Math.Abs(dx);
            
            int d = 2 * a - b;

            int incrE = 2*a;
            int incrNE = 2*(a - b);

            int x = p1.X;
            int y = p1.Y;
            bitmap.SetPixel(x,y,color);

            int s = 1;
            int q = 1;
            if (p1.X > p2.X) q = -1;
            if (p1.Y > p2.Y) s = -1;

            for (int i = 0; i < b; i++)
            {
                if (d >= 0)
                {
                    d += incrNE;
                    y = y + s;
                    x = x + q;
                }
                else
                {
                    if (swaps == 1) y += s;
                    else x += q;
                    d += incrE;
                }
                bitmap.SetPixel(x, y, color);
            }
        }

        private static void Swap(ref int x, ref int y)
        {
            int temp = x;
            x = y;
            y = temp;
        }

        private static int CrossProduct(Point p1, Point p2)
        {
            return p1.X* p2.Y - p1.Y * p2.X;
        }

        private static bool OnRectangle(Point q, Point p1, Point p2)
        {
            return Math.Min(p1.X, p2.X) <= q.X && q.X <= Math.Max(p1.X, p2.X) &&
             Math.Min(p1.Y, p2.Y) <= q.Y && q.Y <= Math.Max(p1.Y, p2.Y);
        }

        private static bool SegmentIntersection(Point P1s, Point P1e, Point P2s, Point P2e)
        {
            int d1 = CrossProduct(new Point(P2e.X - P2s.X, P2e.Y - P2s.Y), new Point(P1s.X - P2s.X, P1s.Y - P2s.Y));
            int d2 = CrossProduct(new Point(P2e.X - P2s.X, P2e.Y - P2s.Y), new Point(P1e.X - P2s.X, P1e.Y - P2s.Y));
            int d3 = CrossProduct(new Point(P1e.X - P1s.X, P1e.Y - P1s.Y), new Point(P2s.X - P1s.X, P2s.Y - P1s.Y));
            int d4 = CrossProduct(new Point(P1e.X - P1s.X, P1e.Y - P1s.Y), new Point(P2e.X - P1s.X, P2e.Y - P1s.Y));
            

            if ((d1 > 0 && d2 > 0) || (d3 > 0 && d4 > 0) || (d1 < 0 && d2 < 0) || (d3 < 0 && d4 < 0)) return false;
            if ((d1 > 0 && d2 < 0) || (d3 > 0 && d4 < 0) || (d1 < 0 && d2 > 0) || (d3 < 0 && d4 > 0)) return true;

            return OnRectangle(P1s, P2s, P2e) || OnRectangle(P1e, P2s, P2e) ||
                OnRectangle(P2s, P1s, P1e) || OnRectangle(P2e, P1s, P1e);
        }

        private static Vector2 GetAB( Point p1, Point p2)
        { // ret.X - a, ret.Y - b
            if (p1.X == 0) // all x's and y's should not be equal to 0
                throw new Exception("Geometry GetAB exception");
            
            double a = ((double)p1.Y - p2.Y) / (p1.X - p2.X);
            double b = p1.Y - a * p1.X;
            
            return new Vector2(a, b);
        }

        private static Point GetClipPoint(Vector2 l1, Vector2 l2)
        { //we have exactly 1 intersection
            int x = (int)((l2.y - l1.y) / (l1.x - l2.x)); //x = (b2 - b1) / (a1 - a2)
            int y = (int)(l1.x * x + l1.y); // y = a1 * x + b1

            if (x == 0) //we dont want 0's and 1 is close enough 
                x = 1;
            if (y == 0)
                y = 1;

            return new Point(x, y);
        }
        private static Point MiddlePoint(Point p1, Point p2, Point p3, Point p4)
        {
            Point p = p1;
            if ((p.Y < p2.Y && p.Y < p3.Y && p.Y < p4.Y) || (p.Y > p2.Y && p.Y > p3.Y && p.Y > p4.Y))
                p = p2;
            else
                return p;
            if ((p.Y < p1.Y && p.Y < p3.Y && p.Y < p4.Y) || (p.Y > p1.Y && p.Y > p3.Y && p.Y > p4.Y))
                return p3;
            return p2;
        }
        public static Point ClipPoint(Point P1s, Point P1e, Point P2s, Point P2e)
        {
            if ((P1s.X == P1e.X && P1s.Y == P1e.Y) || (P2s.X == P2e.X && P2s.Y == P2e.Y))
                return null;
            //first we check if they intersect
            if (!SegmentIntersection(P1s, P1e, P2s, P2e))
                return null;
            //when we know that they intersect
            //1st option - one or both segments are vertical
            if(P1s.X == P1e.X)
            {
                if(P2s.X == P2e.X)
                {
                    return MiddlePoint(P1s, P1e, P2s, P2e);
                }
                Vector2 l = GetAB(P2s, P2e);
                return new Point(P1s.X,(int) (l.x * P1s.X + l.y));
            }
            if(P2s.X == P2e.X)
            {
                Vector2 l = GetAB(P1s, P1e);
                return new Point(P2s.X, (int)(l.x * P2s.X + l.y));
            }
            Vector2 l1 = GetAB(P1s, P1e); //line 1
            Vector2 l2 = GetAB(P2s, P2e); //line 2
            //2nd option - both segments are on the same line
            if (l1.x == l2.x) //p1.a == p2.a since x means a for l1 and l2
                return MiddlePoint(P1s, P1e, P2s, P2e);

            //3rd option - just 1 intersection
            return GetClipPoint(l1, l2);
        }
    }
    public class Vector2
    {
        public double x, y;

        public Vector2(double X, double Y)
        {
            x = X;
            y = Y;
        }
        public static Vector2 operator +(Vector2 v1, Vector2 v2)
        {
            return new Vector2(v1.x + v2.x, v1.y + v2.y);
        }
        public static Vector2 operator -(Vector2 v1, Vector2 v2)
        {
            return new Vector2(v1.x - v2.x, v1.y - v2.y);
        }
    }
    public class Vector3
    {
        public double x, y, z;

        public Vector3(double X, double Y, double Z)
        {
            x = X;
            y = Y;
            z = Z;
        }
        public static Vector3 operator +(Vector3 v1, Vector3 v2)
        {
            return new Vector3(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
        }
        public static Vector3 operator -(Vector3 v1, Vector3 v2)
        {
            return new Vector3(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
        }
        public void Normalize()
        {
            double length = Math.Sqrt(x * x + y * y + z * z);
            x /= length;
            y /= length;
            z /= length;
        }

    }

    public class Point
    {
        public int X, Y;
        public Polygon Owner;
        public Segment Next;
        public Segment Prev;

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public void Move(int dx, int dy)
        {
            X += dx;
            Y += dy;
        }

        public void Draw(Bitmap bitmap, Color color)
        {
            for(int i=-2;i<=2;i++)
                for (int j = -2; j <= 2; j++)
                    bitmap.SetPixel(X+i,Y+j,color);
        }
    }

    public class Segment
    {
        public Point Start, End;

        public double mRev; //mReversed - 1/m - for AET
        public double IntersectionX;

        public Segment(Point start, Point end)
        {
            Start = start;
            start.Next = this;
            End = end;
            end.Prev = this;
        }

        public void Draw(Bitmap bitmap, Color segmentColor, Color pointColor)
        {
            Geometry.Bresenham(Start, End, segmentColor, bitmap);

            //points go after segment
            Start.Draw(bitmap, pointColor);
            End.Draw(bitmap, pointColor);
        }
    }
    class UnfinishedPolygon
    {
        readonly LinkedList<Segment> _segments;
        Segment _unfinishedSegment;

        public UnfinishedPolygon(Point p)
        {
            _segments = new LinkedList<Segment>();
            _unfinishedSegment = new Segment(p, new Point(p.X, p.Y));
        }

        //To call when mouse is moving and point isn't decided yet
        public void UpdateEnd(int x, int y)
        {
            _unfinishedSegment.End.X = x;
            _unfinishedSegment.End.Y = y;
        }

        //To call after mouse click
        public Polygon PointConfirmation()
        {
            if (Geometry.Dist(_unfinishedSegment.Start,_unfinishedSegment.End) < Geometry.Epsilon * Geometry.Epsilon)
                return null;

            if(_segments.Count != 0 && Geometry.Dist(_unfinishedSegment.End,_segments.First().Start) <=Geometry.Epsilon * Geometry.Epsilon)
            {
                if (_segments.Count < 2) return null;
                _unfinishedSegment.End = _segments.First().Start;
                _unfinishedSegment.End.Next = _segments.First();
                _unfinishedSegment.End.Prev = _unfinishedSegment;
                _segments.AddLast(_unfinishedSegment);
                return new Polygon(_segments);
            }
            else
            {
                _segments.AddLast(_unfinishedSegment);
                _unfinishedSegment = new Segment(_unfinishedSegment.End, 
                    new Point(_unfinishedSegment.End.X,_unfinishedSegment.End.Y));
                return null;
            }
        }

        public void Draw(Bitmap bitmap, Color segmentColor, Color pointColor)
        {
            foreach(var s in _segments)
                s.Draw(bitmap, segmentColor, pointColor);
            _unfinishedSegment.Draw(bitmap, segmentColor, pointColor);
        }
    }
    public class Polygon
    {
        readonly LinkedList<Segment> _segments;

        public Color[][] Texture;

        public Vector3[][] NormalVectors;

        public Vector3[][] BumpMap; //height map

        public int Size => _segments.Count; //this is a get property

        public Polygon(LinkedList<Segment> segments)
        {
            _segments = segments;
            foreach (var s in _segments)
            {
                s.Start.Owner = this;
            }
        }

        //////////////////////////////////////////////
        //// getting info
        ////////////////////////////////////////////// 

        public int GetMinX()
        {
            int min = _segments.First.Value.Start.X;
            foreach (var s in _segments)
            {
                if (s.Start.X < min)
                    min = s.Start.X;
            }
            return min;
        }

        public int GetMinY()
        {
            int min = _segments.First.Value.Start.Y;
            foreach (var s in _segments)
            {
                if (s.Start.Y < min)
                    min = s.Start.Y;
            }
            return min;
        }

        public int GetMaxX()
        {
            int max = _segments.First.Value.Start.X;
            foreach (var s in _segments)
            {
                if (s.Start.X > max)
                    max = s.Start.X;
            }
            return max;
        }

        public int GetMaxY()
        {
            int max = _segments.First.Value.Start.Y;
            foreach (var s in _segments)
            {
                if (s.Start.Y > max)
                    max = s.Start.Y;
            }
            return max;
        }
        
        //////////////////////////////////////////////
        //// Basic stuff - moving, removing, drawing etc.
        ////////////////////////////////////////////// 

        public void RemovePoint(Point p)
        {
            if (_segments.Count < 3)
                throw new Exception("Polygon has less than 3 points!");
            if (_segments.Count == 3)
                return;

            for (var s = _segments.First; s != null; s = s.Next)
            {
                if (s.Value.Start == p)
                {
                    if (s == _segments.First)
                    {
                        _segments.RemoveLast();
                        _segments.Last.Value.End.Next = s.Value.Start.Next;
                        s.Value.Start = _segments.Last.Value.End;
                         
                        return;
                    }
                    LinkedListNode<Segment> toRemove = s.Previous;
                    if (toRemove == null || toRemove?.Previous == null && toRemove != _segments.First)
                        throw new Exception("Polygon.RemovePoint unexpected null value"); //if something is wrong with construction of the polygon

                    s.Value.Start = toRemove == _segments.First ? _segments.Last.Value.End : toRemove.Previous.Value.End;
                    s.Value.Start.Next = s.Value;
                    s.List.Remove(toRemove);
                }
            }
        }

        public void Draw(Bitmap bitmap, Color segmentColor, Color brushColor)
        {
            foreach(var s in _segments)
                s.Draw(bitmap, segmentColor, brushColor);
        }

        public LinkedListNode<Segment> GetValidSegment(Point from, int validDist)
        {
            for(LinkedListNode<Segment> s = _segments.First; s!= null; s = s.Next)
            {
                if (Geometry.Dist(from, s.Value) <= validDist * validDist)
                    return s;
            }
            return null;
        }

        public Point GetValidPoint(Point from, int validDist)
        {
            foreach (var s in _segments)
            {
                if (Geometry.Dist(from, s.Start) <= validDist * validDist)
                    return s.Start;
            }
            return null;
        }

        public void MovePolygon(Point p)
        {
            int OffsetX = p.X - _segments.First.Value.Start.X;
            int OffsetY = p.Y - _segments.First.Value.Start.Y;
            foreach (var s in _segments)
            {
                int tryx = s.Start.X + OffsetX;
                int tryy = s.Start.Y + OffsetY;
                if (tryx <= 3 )
                    OffsetX = 3 - s.Start.X;
                if (tryx > MainWindow.MaxX - 2)
                    OffsetX = MainWindow.MaxX - 3 - s.Start.X;
                if (tryy <= 3)
                    OffsetY = 3 - s.Start.Y;
                if (tryy > MainWindow.MaxY - 2)
                    OffsetY = MainWindow.MaxY - 3 - s.Start.Y;
            }

            foreach (var s in _segments)
            {
                s.Start.X += OffsetX;
                s.Start.Y += OffsetY;
            }
        }

        public Polygon Clone()
        {
            LinkedList<Segment> segments = new LinkedList<Segment>();
            Point firstStart = new Point(_segments.First.Value.Start.X, _segments.First.Value.Start.Y);
            Point prevEnd = firstStart;
            for (var s = _segments.First; s.Next != null; s = s.Next)
            {
                Point start = prevEnd;
                prevEnd = new Point(s.Value.End.X, s.Value.End.Y);
                Segment ss = new Segment(start, prevEnd);
                segments.AddLast(ss);
            }
            Segment last = new Segment(prevEnd, firstStart);
            segments.AddLast(last);

            return new Polygon(segments);
        }
        
        //////////////////////////////////////////////
        //// Filling
        ////////////////////////////////////////////// 
        
        Point[] VertexSort()
        { // y-ascending order
            Point[] arr = new Point[Size];

            int i = 0;
            foreach (var s in _segments)
            {
                Point p = s.Start;
                if (i == 0)
                    arr[i++] = p;
                else
                {
                    for (int j = i; j >= 0; j--)
                    {
                        if (j == 0 || p.Y >= arr[j - 1].Y)
                        {
                            arr[j] = p;
                            break;
                        }
                        else
                            arr[j] = arr[j - 1];
                    }
                    i++;
                }
            }
            return arr;
        }

        double GetM(Point p1, Point p2)
        {
            return ((double)p1.Y - p2.Y)/(p1.X - p2.X);
        }

        Color GetColor(Color lightColor,int x, int y)
        {
            Vector3 lightPos = Geometry.LightPos;

            Vector3 light = new Vector3(lightPos.x - x, lightPos.y - y, lightPos.z);
            light.Normalize();

            Color textureColor = Texture[x][y];
            Vector3 normal;

            if (NormalVectors == null)
                normal = new Vector3(0,0,1);
            else
                normal = NormalVectors[x][y];
            if (BumpMap != null)
            {
                normal += BumpMap[x][y];
                normal.Normalize();
            }
                

            int FinalR = (int)(Geometry.LambHelp * lightColor.R * textureColor.R * Geometry.Cos(normal, light));
            if (FinalR > 255) FinalR = 255;
            int FinalG = (int)(Geometry.LambHelp * lightColor.G * textureColor.G * Geometry.Cos(normal, light));
            if (FinalG > 255) FinalG = 255;
            int FinalB = (int)(Geometry.LambHelp * lightColor.B * textureColor.B * Geometry.Cos(normal, light));
            if (FinalB > 255) FinalB = 255;

            return Color.FromArgb(255, FinalR, FinalG, FinalB);
        }

        public void ScanLineAlgorithm(Bitmap bitmap, Color lightColor)
        {
            if (Texture == null)
                return;

            Point[] sortedVertices = VertexSort();

            int ymin = sortedVertices[0].Y;
            int ymax = sortedVertices[sortedVertices.Length - 1].Y;

            List<Segment> AET = new List<Segment>();
            List<int> intersectionsX = new List<int>();

            for (int y = ymin; y < ymax; y++)
            {
                //Update AET
                foreach (var p in sortedVertices)
                {
                    if (p.Y == y)
                    {
                        if (p.Prev.Start.Y >= p.Y && p.Prev.Start.Y != p.Prev.End.Y)
                        {
                            p.Prev.IntersectionX = p.X;
                            p.Prev.mRev = 1/GetM(p.Prev.Start, p.Prev.End);
                            if(!AET.Contains(p.Prev))
                                AET.Add(p.Prev); 
                        }
                        else
                            AET.Remove(p.Prev);

                        if (p.Next.End.Y >= p.Y && p.Next.Start.Y != p.Next.End.Y)
                        {
                            p.Next.IntersectionX = p.X;
                            p.Next.mRev = 1 / GetM(p.Next.End, p.Next.Start);
                            if (!AET.Contains(p.Next))
                                AET.Add(p.Next);
                        }
                        else
                            AET.Remove(p.Next);
                    }
                }
                AET.Sort((o, o2) => (int)o.IntersectionX - (int)o2.IntersectionX);
                //drawing
                for (int i = 0; i < AET.Count; i += 2)
                    for (int x = (int) AET[i].IntersectionX; x <= (int) AET[i + 1].IntersectionX; x++)
                    {
                        Color newCol = GetColor(lightColor, x, y);
                        bitmap.SetPixel(x,y, newCol);
                    }
                foreach (var s in AET)
                    s.IntersectionX += s.mRev;
            }
        }
        

        //////////////////////////////////////////////
        //// Clipping
        ////////////////////////////////////////////// 

        private bool HasPointInside(Point p)
        {
            int count = 0;
            Point P = new Point(p.X, 0);
            foreach (var s in _segments)
            {
                if (Geometry.ClipPoint(p, P, s.Start, s.End) != null)
                {
                    if (s.Start.X == s.End.X && s.End.X == p.X)
                    {
                        Segment a = s.Start.Prev;
                        Segment b = s.End.Next;
                        Point x = a.Start;
                        Point y = b.End;

                        if ((x.X < p.X && y.X > p.X) || (x.X > p.X && y.X < p.X))
                            count++;
                        continue;
                    }
                    if (s.Start.X == p.X)
                    {
                        Segment a = s.Start.Prev;
                        Segment b = s.Start.Next;
                        Point x = a.Start;
                        Point y = b.End;

                        if ((x.X < p.X && y.X > p.X) || (x.X > p.X && y.X < p.X))
                            count++;
                        continue;
                    }
                    count++;
                }
            }
            if (count % 2 == 0)
                return false;
            else
                return true;
        }

        public LinkedList<Point> VerticesNumbering()
        { //counter clockwise
            LinkedList<Point> ret = new LinkedList<Point>();
            int sum = 0;
            foreach(var s in _segments)
                sum += (s.End.X - s.Start.X) * (s.End.Y + s.Start.Y);
            if (sum > 0)
                foreach (var s in _segments)
                    ret.AddLast(s.Start);
            else
                for (int i = _segments.Count - 1; i >= 0; i--)
                    ret.AddLast(_segments.ElementAt(i).Start);
            return ret;
        }

        public List<Polygon> ClipPolygon(Polygon p)
        {
            List<Polygon> ret = new List<Polygon>();
            LinkedList<Point> points1 = VerticesNumbering();
            LinkedList<Point> points2 = p.VerticesNumbering();

            LinkedList<Point> ClipPoints = new LinkedList<Point>();

            //Adding clip points to polygon points
            for (var t1 = points1.First; t1 != null; t1 = t1.Next)
                for (var t2 = points2.First; t2 != null; t2 = t2.Next)
                {

                    Point P1s = t1.Value;
                    Point P1e = t1.Next != null ? t1.Next.Value : points1.First.Value;
                    Point P2s = t2.Value;
                    Point P2e = t2.Next != null ? t2.Next.Value : points2.First.Value;

                    //if this condition is true, both points are 1 clip point which has been already added
                    if (P1s == P2s || P1s == P2e || P1e == P2s || P1e == P2e)
                        continue;

                    Point clip = Geometry.ClipPoint(P1s, P1e, P2s, P2e);
                    if (clip != null)
                    {
                        points1.AddAfter(t1, clip);
                        points2.AddAfter(t2, clip);
                        ClipPoints.AddLast(clip);
                        t2 = t2.Next;
                    }
                }
            //we have list of clip points
            //if list is empty we can have situation when 1 polygon is inside of the other
            if(ClipPoints.Count == 0)
            {
                if (HasPointInside(p._segments.First.Value.Start))
                    return new List<Polygon>() { p };
                if (p.HasPointInside(_segments.First.Value.Start))
                    return new List<Polygon>() { this };
                return null;
            }


            //let's make list of entry clip points

            LinkedList<Point> EntryClipPoints = new LinkedList<Point>();

            bool isEntry = true;

            //if points1.First is inside polygon created by Polygon p, then first clipPoint counting from points1.First
            //is not an entryClipPoint
            if (p.HasPointInside(points1.First.Value))
                isEntry = false;


            for (var t1 = points1.First; t1 != null; t1 = t1.Next)
            {
                if (!ClipPoints.Contains(t1.Value))
                    continue;
                if (isEntry)
                {
                    EntryClipPoints.AddLast(t1.Value);
                    isEntry = false;
                }
                else
                    isEntry = true;
            }
            //we have complete list of entry clip points
            LinkedList<Segment> segments = new LinkedList<Segment>();
            LinkedList<Point> polygon = points1;

            LinkedListNode<Point> firstPoint = null;

            while(EntryClipPoints.Count > 0)
            {
                polygon = points1;
                firstPoint = polygon.Find(EntryClipPoints.First.Value);
                EntryClipPoints.Remove(firstPoint.Value);
                var t2 = firstPoint.Next == null ? polygon.First : firstPoint.Next;
                if (t2.Previous == null)
                    segments.AddLast(new Segment(polygon.Last.Value, t2.Value));
                else
                    segments.AddLast(new Segment(t2.Previous.Value, t2.Value));
                while (1 == 1)
                {
                    if (t2.Value == firstPoint.Value)
                    {
                        ret.Add(new Polygon(segments));
                        firstPoint = null;
                        segments = new LinkedList<Segment>();
                        break; // we're getting next iteration for t
                    }
                    if (ClipPoints.Contains(t2.Value))
                    { // we change polygon
                        polygon = polygon == points1 ? points2 : points1;
                        t2 = polygon.Find(t2.Value);
                    }
                    if (EntryClipPoints.Contains(t2.Value))
                        EntryClipPoints.Remove(t2.Value); // we're getting rid of it
                    t2 = t2.Next == null ? polygon.First : t2.Next;
                    if (t2.Previous == null)
                        segments.AddLast(new Segment(polygon.Last.Value, t2.Value));
                    else
                        segments.AddLast(new Segment(t2.Previous.Value, t2.Value));
                }
            }
            
            return ret;
        }// ClipPolygon
    }
}