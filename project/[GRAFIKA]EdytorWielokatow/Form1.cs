﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace _GRAFIKA_EdytorWielokatow
{
    public partial class MainWindow : Form
    {
        private readonly Form _form;
        Graphics _graphics;
        readonly Color[] _colors = new Color[4];
        private Bitmap b;
        private Color[][] texture;
        private static int WindowMaxX = 1280;
        public static int MaxX { get { return WindowMaxX;} }
        private static int WindowMaxY = 720;
        public static int MaxY { get { return WindowMaxY; } }
        private Color lightColor;


        public MainWindow()
        {
            InitializeComponent();
            this.MaximumSize = new Size(WindowMaxX,WindowMaxY);


            ToolTip toolTip = new ToolTip();
            toolTip.SetToolTip(ToolSelection, "Interaction mode");
            toolTip.SetToolTip(ToolCreation, "Creation mode");

            lightColor = Color.White;
            _colors[0] = Color.Black;
            _colors[1] = Color.Blue;
            _colors[2] = Color.OrangeRed;
            _colors[3] = Color.DarkRed;
            _form = this;

            b = new Bitmap(WindowMaxX, WindowMaxY);
            pictureBox.Image = b;

            KeyPreview = true;
            KeyDown += MyForm_KeyDown;

            //Double buffering property is private for panel so i have to do a trick
            typeof(PictureBox).InvokeMember("DoubleBuffered", BindingFlags.SetProperty
            | BindingFlags.Instance | BindingFlags.NonPublic, null,
            MainPanels.Panel2, new object[] { true });

            

            //those fps are a lie... somehow canvas is refreshed more often
            int frames = 1000/40; //40 fps

            Timer timer1 = new Timer {Interval = frames};
            timer1.Tick += timer1_Tick;
            timer1.Start();
        }
        private void timer1_Tick(object sender, EventArgs e)
        { 
            RefreshCanvas();
        }

        void RefreshCanvas()
        {
            MainPanels.Panel2.Refresh();
        }


        ////////////////////////
        //Tool Panel Methods
        ////////////////////////

        private Button _selectedTool;
        //For management of SelectedTool variable and flat styles
        private void ToolMaintenance(object sender)
        {
            if (_selectedTool == sender as Button) return;
            ((Button) sender).FlatStyle = FlatStyle.Flat;
            _selectedTool = (sender as Button);
        }
        
        private void ToolSelection_Click(object sender, EventArgs e)
        {
            ToolMaintenance(sender);
            Overseer.SelectedWorkMode = WorkMode.Selection;
            Overseer.ResetUnfinishedOne();
        }
        private void ToolCreation_Click(object sender, EventArgs e)
        {
            ToolMaintenance(sender);
            Overseer.SelectedWorkMode = WorkMode.Create;
        }

        ////////////////////////
        //Events
        ////////////////////////
        /// 
           

        private void CanvasPaint(object sender, PaintEventArgs e)
        {
            int x = Cursor.Position.X - MainPanels.Left - MainPanels.Panel2.Left - 
                      _form.Location.X - MainPanels.SplitterWidth - 7;
            int y = Cursor.Position.Y - MainPanels.Top - MainPanels.Panel2.Top - _form.Location.Y - 30;

            if (x < 3 || y < 3 || x > WindowMaxX - 3 || y > WindowMaxY - 3)
                return;

            Overseer.MoveUnfinishedEnd(x, y);
            Overseer.DragPoint(new Point(x,y));
            
            _graphics = Graphics.FromImage(((PictureBox) sender).Image);
            _graphics.Clear(Color.LightGray);
            Image image = ((PictureBox)sender).Image;
            
            b =  Overseer.DrawPolygons(b, image, _colors, texture, lightColor);
            ((PictureBox) sender).Image = b;
        }
       
        private void CanvasClick(object sender, EventArgs e)
        {
            switch(Overseer.SelectedWorkMode)
            {
                case WorkMode.Create:
                    {
                        int x = ((MouseEventArgs)e).X;
                        int y = ((MouseEventArgs)e).Y;
                        
                        Overseer.ConfirmPointUnfinished(x,y);
                        break;
                    }
                case WorkMode.Selection:
                    {
                        int x = ((MouseEventArgs)e).X;
                        int y = ((MouseEventArgs)e).Y;
                        Overseer.SelectSegment(x,y);
                        Overseer.SelectPoint(x, y);
                        break;
                    }
                case WorkMode.Move:
                {
                    Overseer.StopMove();
                    break;
                }
                case WorkMode.Clip:
                {
                    int x = ((MouseEventArgs)e).X;
                    int y = ((MouseEventArgs)e).Y;
                    Overseer.Clip(x,y);
                    break;
                }
                default:
                    return;
            }
        }

        private void Canvas_DoubleClick(object sender, EventArgs e)
        {
            switch (Overseer.SelectedWorkMode)
            {
                case WorkMode.Selection:
                    {
                        int x = ((MouseEventArgs)e).X;
                        int y = ((MouseEventArgs)e).Y;
                        Overseer.SelectSegment(x,y);
                        Overseer.CreateNewPoint(x,y);
                        break;
                    }
                default:
                    return;
            }
        }

        ////////////////////////
        //Keyboard reaction
        ////////////////////////
        private void MyForm_KeyDown(object sender, KeyEventArgs e)
        {
            switch (Overseer.SelectedWorkMode)
            {
                case WorkMode.Selection:
                {
                    switch (e.KeyCode)
                    {
                        case Keys.A:
                            Overseer.SelectWhole = !Overseer.SelectWhole;
                            Overseer.ClearSelection();
                            break;
                        case Keys.C:
                            Overseer.BeginClip();
                            break;
                        case Keys.D:
                            if (Overseer.SelectWhole)
                                Overseer.RemoveSelectedPolygon();
                            else
                                Overseer.RemovePoint();
                            break;
                        case Keys.M:
                            Overseer.BeginMove();
                            break;
                        case Keys.I:
                            Overseer.MoveLight(Direction.Up);
                            break;
                        case Keys.K:
                            Overseer.MoveLight(Direction.Down);
                            break;
                        case Keys.J:
                            Overseer.MoveLight(Direction.Left);
                            break;
                        case Keys.L:
                            Overseer.MoveLight(Direction.Right);
                            break;
                        case Keys.Add:
                            Overseer.MoveLight(Direction.Below);
                            break;
                        case Keys.Subtract:
                            Overseer.MoveLight(Direction.Above);
                            break;
                        }
                    break;
                }
                case WorkMode.Move:
                {
                    if (e.KeyCode == Keys.M)
                    {
                        Overseer.StopMove();
                    }
                    break;
                }
                default:
                    return;
            }
        }

        private Bitmap ConvertToBitmap(string fileName)
        {
            Bitmap bitmap;
            using (Stream bmpStream = System.IO.File.Open(fileName, System.IO.FileMode.Open))
            {
                Image image = Image.FromStream(bmpStream);

                bitmap = new Bitmap(image);

            }
            return bitmap;
        }

        private Color[][] ConvertBitmapToTexture(Bitmap b)
        {
            Color[][] c = new Color[WindowMaxX][];
            for(int i = 0 ;i < WindowMaxX; i++)
            {
                c[i] = new Color[WindowMaxY];
                for (int j = 0; j < WindowMaxY; j++)
                    c[i][j] = b.GetPixel(i % b.Size.Width, j % b.Size.Height);
            }
            return c;
        }

        private Vector3[][] ConvertBitmapToVectors(Bitmap b)
        {
            Vector3[][] v = new Vector3[WindowMaxX][];
            for (int i = 0; i < WindowMaxX; i++)
            {
                v[i] = new Vector3[WindowMaxY];
                for (int j = 0; j < WindowMaxY; j++)
                {
                    Color c = b.GetPixel(i % b.Size.Width, j % b.Size.Height);
                    double x = (double)c.R / 127.5 - 1;
                    double y = (double)c.G / 127.5 - 1;
                    double z = (double)c.B / 255;
                    v[i][j] = new Vector3(x,y,z);
                }
            }
            return v;
        }

        private double[][] ConvertBitmapToHeights(Bitmap b)
        {
            double[][] ret = new double[WindowMaxX][];
            for (int i = 0; i < WindowMaxX; i++)
            {
                ret[i] = new double[WindowMaxY];
                for (int j = 0; j < WindowMaxY; j++)
                    ret[i][j] = b.GetPixel(i % b.Size.Width, j % b.Size.Height).R / (double)255;
            }
            return ret;
        }

        private Vector3[][] Blinn(double[][] h)
        {
            Vector3[][] ret = new Vector3[h.Length][];
            
            for(int i = 0; i <h.Length; i++)
            {
                ret[i] = new Vector3[h[i].Length];
                for(int j = 0; j < h[i].Length; j++)
                {
                    double Hg = h[i][j];
                    double Ha = h[i + 1 < h.Length ? i+1 : i][j];
                    double Hr = h[i][j + 1 < h[i].Length ? j+1: j];

                    Vector3 diff1 = new Vector3(1, 0, Hr - Hg);
                    Vector3 diff2 = new Vector3(0, 1, Ha - Hg);

                    ret[i][j] = new Vector3(Hg - Ha, Hg - Hr, 1);
                    ret[i][j].Normalize();
                }
            }
            return ret;
        }

        private void FillTextureButton_Click(object sender, EventArgs e)
        {
            var d = new OpenFileDialog
            {
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures)
            };

            if (d.ShowDialog(this) == DialogResult.OK)
            {
                try
                {
                    Bitmap b = ConvertToBitmap(d.FileName);
                    Color[][] txture = ConvertBitmapToTexture(b);

                    Overseer.ChooseTexture(txture);
                }
                catch (Exception exc)
                { //wrong file type exception
                }

            }
        }

        private void HeightMapButton_Click(object sender, EventArgs e)
        {
            var d = new OpenFileDialog
            {
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures)
            };

            if (d.ShowDialog(this) == DialogResult.OK)
            {

                try
                {
                    Bitmap b = ConvertToBitmap(d.FileName);
                    double[][] Heights = ConvertBitmapToHeights(b);


                    Overseer.ChooseHeightMap(Blinn(Heights));
                }
                catch (Exception exc)
                { //wrong file type exception
                }
            }
        }

        private void NormalVectorButton_Click(object sender, EventArgs e)
        {
            var d = new OpenFileDialog
            {
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures)
            };

            if (d.ShowDialog(this) == DialogResult.OK)
            {
                
                try
                {
                    Bitmap b = ConvertToBitmap(d.FileName);
                    Vector3[][] Normals = ConvertBitmapToVectors(b);

                    Overseer.ChooseNormalMap(Normals);
                }
                catch (Exception exc)
                { //wrong file type exception
                }
            }
        }

        private void LightColorButton_Click(object sender, EventArgs e)
        {
            //colordialog
            ColorDialog d = new ColorDialog();
            if (d.ShowDialog() == DialogResult.OK)
            {
                lightColor = d.Color;
            }
        }
    }
}